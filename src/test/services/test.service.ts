import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Test } from '../shared/test.class';
import { TestEntity } from '../shared/test.entity';

@Injectable()
export class TestService {
    constructor(
        @InjectRepository(TestEntity)
        private readonly testRepository: Repository<TestEntity>,
    ) {}

    async save (test: Test): Promise<Test> {
        return this.testRepository.save(test);
    }

    async findAll (): Promise<Test[]> {
        return this.testRepository.find();
    }
}
