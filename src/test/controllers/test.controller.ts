import { Body, Controller, Get, Post } from '@nestjs/common';
import { TestService } from '../services/test.service';
import { Test } from '../shared/test.class';

@Controller('/test')
export class TestController {
    constructor(
        private readonly testService: TestService
    ) {}


    @Post('/save')
    async save(@Body() test: Test): Promise<any> {
        return this.testService.save(test);
    }

    @Get('/find')
    async findAll(): Promise<any> {
        return this.testService.findAll();
    }
}
