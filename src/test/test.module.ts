import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { TestEntity } from "./shared/test.entity";
import { TestService } from './services/test.service';
import { TestController } from './controllers/test.controller';

@Module({
    imports: [
        TypeOrmModule.forFeature([TestEntity])
    ],
    controllers: [TestController],
    providers: [TestService],
})
export class TestModule { }