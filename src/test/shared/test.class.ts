import { ApiProperty } from "@nestjs/swagger";

export class Test {
    id?: number;
    @ApiProperty()
    name: string;
}