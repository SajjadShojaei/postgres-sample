import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostgresConfiguration } from './postgres.config.service';

@Module({
    imports: [ ConfigModule.forRoot({isGlobal: true}),
        TypeOrmModule.forRootAsync({ useClass: PostgresConfiguration })]
})
export class DatabaseModule { }
