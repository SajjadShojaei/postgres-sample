import { TypeOrmModule, TypeOrmModuleOptions, TypeOrmOptionsFactory } from "@nestjs/typeorm";

export class PostgresConfiguration implements TypeOrmOptionsFactory {
    createTypeOrmOptions(connectionName?: string): TypeOrmModuleOptions | Promise<TypeOrmModuleOptions> {
        const options: TypeOrmModuleOptions = {
            type: "postgres",
            //url: process.env.DATABASE_URL,
            host: "postgres_container", // container name
            port: +process.env.POSTGRES_PORT || 5432,
            username: "admin",
            password: "secret",
            database: "postgres_db",
            entities: [__dirname + '/../**/*.entity{.ts,.js}'],
            autoLoadEntities:true,
            synchronize: true

        };
        return options
    }
}